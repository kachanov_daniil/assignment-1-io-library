section .data
 %define STDOUT 1
 %define SYSCALL_WRITE 1
 %define SYSCALL_EXIT 60
 %define STDIN 0

section .text
    global _start

exit:
    mov rax, SYSCALL_EXIT
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину;
string_length:
    mov rax, rdi
.counter :
    cmp byte [rdi], 0
    je .end_string
    inc rdi
    jmp .counter
.end_string:
    sub rdi, rax
    mov rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax;
    mov rax, SYSCALL_WRITE ;
    mov rdi, STDOUT;
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    xor rax, rax
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int : 
    xor rax, rax
    mov rax, rdi
    cmp rax, 0
    jge .positive
    push rax
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    pop rax
    neg rax
.positive:
    mov rdi, rax
print_uint: ; Выводит беззнаковое 8-байтовое число в десятичном формате
   push rax
   push rdi
   mov rax, rdi
   mov r8, 10
   push 0
.uint_loop:
   xor rdx, rdx
   div r8
   add rdx, '0'
   push rdx
   test rax, rax
   jnz .uint_loop
.print_loop:
   cmp byte[rsp], 0
   je .print_end
   mov dil, [rsp]
   call print_char
   add rsp, 8
   jmp .print_loop
.print_end:
   pop rdi
   pop rdi
   pop rax
   ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1, если они равны, 0 иначе
string_equals: 
    push rdi
    push rdx
    push rsi
    xor rax, rax
.loop:
    cmp byte [rdi], 0
    je .equals
    mov al, [rdi]
    cmp al, [rsi]
    jne .notequals
    inc rdi
    inc rsi
    jmp .loop
.equals:
    cmp byte [rsi], 0
    jne .notequals
    mov rax, 1
    jmp .end_equals
.notequals:
    mov rax, 0
.end_equals:
    pop rsi
    pop rdx
    pop rdi
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rcx
    xor rax, rax
    mov rdi, STDIN
    mov rdx, 1
    mov rsi, rsp
    syscall
    cmp rax, 0
    je .end_reading_char
    mov al, [rsp]
.end_reading_char:
    pop rcx
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале.
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    test rsi, rsi
    jz .buffer_error
    mov r8, rsi
    add r8, rdi
    mov rsi, rdi
    mov rcx, rsi
    xor rax, rax
.read_loop:
    push rdx
    push rsi
    push rdi
    push rcx
    push r8
    call read_char
    pop r8
    pop rcx
    pop rdi
    pop rsi
    pop rdx

    ; Пропуск пробельных символов в начале
    cmp al, 0x20
    je .skip
    cmp al, 0x9
    je .read_loop
    cmp al, 0xA
    je .skip
    
    test rax, rax
    jz .word_found
    cmp rcx, r8
    jae .buffer_error    
    mov byte[rcx], al
    inc rcx
    jmp .read_loop

.word_found:
    mov byte[rcx], 0
    mov rax, rdi
    sub rcx, rdi
    mov rdx, rcx
    ret

.buffer_error:
    xor rax, rax
    xor rdx, rdx
    ret

.skip:
    cmp rcx, rdi ; Если указатель на пробел находится в начале, то пропускаем пробел, иначе заканчиваем ввод слова
    je .read_loop
    jmp .word_found
    



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx: его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint :
    push rdi
    push rsi
    xor rax, rax
    xor rcx, rcx
.parse_loop:
    cmp byte [rdi], 0
    je .end_parse
    mov rcx, [rdi]
    cmp cl, '0'
    jl .invalid_digit
    cmp cl, '9'
    jg .invalid_digit
    sub rcx, '0'    
    push rdi
    mov rdi, 10
    push rdx
    imul rdi
    pop rdx
    add al, cl
    pop rdi
    inc rdi
    inc rdx
    jmp .parse_loop

.invalid_digit:
.end_parse:
    pop rsi
    pop rdi
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx: его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int :
    push rdi
    push rsi
    xor rax, rax
    xor rcx, rcx
    mov rcx, [rdi]
    cmp cl, '-'
    je .negative
    cmp cl, '+'
    je .positive
    call parse_uint
    jmp .end_parse
.positive:
    inc rdi
    push rcx
    call parse_uint
    pop rcx
    inc rdx
    jmp .end_parse
.negative:
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .end_parse
    neg rax
    inc rdx
.end_parse:
    pop rsi
    pop rdi
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
section .text
string_copy:
    xor rax, rax
    xor rcx, rcx
    push rdi
    push rsi
.copy_loop:
    mov al, [rdi + rcx]
    cmp rcx, rdx
    jae .buffer_full
    mov [rsi + rcx], al
    cmp al, 0
    je .end_copy
    inc rcx
    jmp .copy_loop

.buffer_full: 
    xor rax, rax
    jmp .end_copy

.success:
    mov byte[rsi + rcx + 1], 0
    mov rax, rcx
.end_copy:
    pop rsi
    pop rdi
    ret

